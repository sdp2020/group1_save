---
title: Evaluation
subtitle: How Well Does Our System Perform?
comments: false
---

## Evaluation Tests

Show the results of testing where available. This can include tables, graphs, videos etc. Be careful to present data in an appropriate manner and be clear about the conclusions drawn from whatever it is you present

### User Tests

Present any user testing you have performed for your product. The same advice about presenting data should be used as above

## Main Areas of Improvement

What did you learn from your testing? How did your evaluation/user testing change your system?
